<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\ModelBarang;
use Carbon\Carbon;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ModelBarang::all();
        return view('barang',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('barang_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new ModelBarang();
        $data->kode_barang = carbon::now()->timestamp;
        $data->nama_barang = $request->nama;
        $data->deskripsi = $request->deskripsi;
        $data->stok_barang = $request->stok;
        $data->harga_barang = $request->harga;
        $data->save();
        return redirect()->route('barang.index')->with('alert-success','Data berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ModelBarang::where('id',$id)->get();

        return view('barang_edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ModelBarang::where('id',$id)->first();
        // $data->kode_barang = $request->kode;
        $data->nama_barang = $request->nama;
        $data->deskripsi = $request->deskripsi;
        $data->stok_barang = $request->stok;
        $data->harga_barang = $request->harga;
        $data->save();
        return redirect()->route('barang.index')->with('alert-success','Data berhasil diubah!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ModelBarang::where('id',$id)->first();
        $data->delete();
        return redirect()->route('barang.index')->with('alert-success','Data berhasi dihapus!');
    }
}
