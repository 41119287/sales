@extends('base')
@section('content')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <hr>
            @foreach($data as $datas)
            <form action="{{ route('lokasi.update', $datas->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="kode">Kode Outlet:</label>
                    <input type="text" class="form-control" id="kode" name="kode" value="{{ $datas->kode_lokasi}}">
                </div>
                <div class="form-group">
                    <label for="nama">Nama Outlet:</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ $datas->nama_lokasi }}">
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <a href="{{ url('/lokasi') }}" class="btn btn-md btn-danger">Cancel</a>
                </div>
            </form>
            @endforeach
        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection