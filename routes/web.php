<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('kontak','Kontak');
Route::resource('lokasi','LokasiController');
Route::resource('pengiriman','PengirimanController');
Route::resource('barang','BarangController');


Route::get('/', function () {
    return view('index');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
